package main

import "encoding/json"

type Message struct {
	MessageId int64 `json:"message_id"`
	Chat      struct {
		Id int64 `json:"id"`
	} `json:"chat"`
	Text string `json:"text"`
}

type Update struct {
	UpdateId int64    `json:"update_id"`
	Message  *Message `json:"message"`
}

type APIResponse struct {
	Ok          bool            `json:"ok"`
	Description string          `json:"description"`
	Result      json.RawMessage `json:"result"`
}
