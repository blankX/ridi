package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func parseBody(resp *http.Response) (json.RawMessage, error) {
	defer resp.Body.Close()
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read response body: %s", err)
	}
	var v APIResponse
	err = json.Unmarshal(bytes, &v)
	if err != nil {
		return nil, fmt.Errorf("failed to parse response body: %s", err)
	}
	if !v.Ok {
		return nil, fmt.Errorf("api returned error: %s", v.Description)
	}
	return v.Result, nil
}

func chatIsAllowed(allowedChats []int64, chatId int64) bool {
	for i := 0; i < len(allowedChats); i++ {
		if allowedChats[i] == chatId {
			return true
		}
	}
	return false
}

func getUpdates(client *http.Client, botToken string, timeout, offset int64) ([]Update, error) {
	url := fmt.Sprintf("https://api.telegram.org/bot%s/getUpdates?allowed_updates=[\"message\"]", botToken)
	if timeout != 0 {
		url = fmt.Sprintf("%s&timeout=%d", url, timeout)
	}
	if offset > 0 {
		url = fmt.Sprintf("%s&offset=%d", url, offset)
	}
	resp, err := client.Get(url)
	if err != nil {
		return nil, fmt.Errorf("failed to get updates: %s", err)
	}
	rawJson, err := parseBody(resp)
	if err != nil {
		return nil, fmt.Errorf("failed to parse getUpdates response: %s", err)
	}
	rawJsonBytes, err := rawJson.MarshalJSON()
	if err != nil {
		return nil, fmt.Errorf("failed to marshal raw json bytes for getUpdates response: %s", err)
	}
	var updates []Update
	err = json.Unmarshal(rawJsonBytes, &updates)
	if err != nil {
		return nil, fmt.Errorf("failed to parse raw json bytes for getUpdates response: %s", err)
	}
	return updates, nil
}
